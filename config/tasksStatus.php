<?php

return [
    'todo' => 'Sin empezar',
    'inProgress' => 'En curso',
    'done' => 'Completada'
];
