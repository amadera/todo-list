<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Tasks App</title>

        <!-- CSS And JavaScript -->
        <link rel="stylesheet" type="text/css" href="css/app.css">
    </head>

    <body>
        <div class="container">
            <nav class="navbar navbar-default">
                <!-- Navbar Contents -->
            </nav>
        </div>

        <div class="container">
            @yield('content')
        </div>
    </body>
</html>
