<!-- resources/views/tasks.blade.php -->

@extends('layouts.app')

@section('content')

    <!-- Bootstrap Boilerplate... -->

    <div class="panel-body">
        <!-- Display Validation Errors -->
        @include('common.errors')

        <header>
            <div class="date">
                {{date("l, M d")}}
            </div>
            <div>
                {{count($tasks)}}
                @if (count($tasks) == 1) task @endif
                @if (count($tasks) != 1) tasks @endif
            </div>
        </header>

        <!-- New Task Form -->
        <form action="{{ url('task') }}" method="POST" class="form-horizontal">
            {{ csrf_field() }}

            <!-- Task Name -->
            <div class="form-group">
                    <input type="text" name="name" id="task-name" class="form-control">

                    <div class="custom-select">
                        <select name="status" id="task-status">
                            @foreach(config('tasksStatus') as $status => $value)
                                <option value="{{ $status }}">{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-plus"></i> Add Task
                    </button>
            </div>
        </form>
    </div>

    <!-- TODO: Current Tasks -->
    @if (count($tasks) > 0)
        <div class="panel panel-default">

            <div class="panel-body">
                <table class="table task-table">

                    <!-- Table Body -->
                    <tbody>
                        @foreach ($tasks as $task)
                            <tr>
                                <!-- Task Name -->
                                <td class="table-text">
                                    @if ($task->status != 'done')
                                        <div>{{ $task->name }}</div>
                                    @endif

                                    @if ($task->status == 'done')
                                        <div class="is-task-done">{{ $task->name }}</div>
                                    @endif
                                </td>
                                <td class="table-text">
                                    <div>
                                    <form action="{{ url('updateTask/'.$task->id) }}" class="form-save" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('PUT') }}
                                        <div class="custom-select">
                                        <select name="status" id="task-status">
                                            @foreach(config('tasksStatus') as $status => $value)
                                                <option value="{{ $status }}"
                                                @if ($status == $task->status)
                                                    selected="selected"
                                                @endif
                                                >{{ $value }}</option>
                                            @endforeach
                                        <select>
                                    </div>

                                        <input type="hidden" name="_method" value="PUT">
                                        <button type="submit" class="btn btn-default">
                                            <svg class="svg-icon-save" viewBox="0 0 20 20" fill="#fff">
                                            <path d="M17.064,4.656l-2.05-2.035C14.936,2.544,14.831,2.5,14.721,2.5H3.854c-0.229,0-0.417,0.188-0.417,0.417v14.167c0,0.229,0.188,0.417,0.417,0.417h12.917c0.229,0,0.416-0.188,0.416-0.417V4.952C17.188,4.84,17.144,4.733,17.064,4.656M6.354,3.333h7.917V10H6.354V3.333z M16.354,16.667H4.271V3.333h1.25v7.083c0,0.229,0.188,0.417,0.417,0.417h8.75c0.229,0,0.416-0.188,0.416-0.417V3.886l1.25,1.239V16.667z M13.402,4.688v3.958c0,0.229-0.186,0.417-0.417,0.417c-0.229,0-0.417-0.188-0.417-0.417V4.688c0-0.229,0.188-0.417,0.417-0.417C13.217,4.271,13.402,4.458,13.402,4.688"></path>                                            </svg>
                                        </button>
                                    </form>
                                    </div>
                                </td>
                                    <!-- Delete Button -->
                                <td class="delete-btn-row">
                                    <form action="{{ url('task/'.$task->id) }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button type="submit" class="delete-btn">
                                            <svg class="svg-icon" viewBox="0 0 20 20">
                                                <path d="M17.114,3.923h-4.589V2.427c0-0.252-0.207-0.459-0.46-0.459H7.935c-0.252,0-0.459,0.207-0.459,0.459v1.496h-4.59c-0.252,0-0.459,0.205-0.459,0.459c0,0.252,0.207,0.459,0.459,0.459h1.51v12.732c0,0.252,0.207,0.459,0.459,0.459h10.29c0.254,0,0.459-0.207,0.459-0.459V4.841h1.511c0.252,0,0.459-0.207,0.459-0.459C17.573,4.127,17.366,3.923,17.114,3.923M8.394,2.886h3.214v0.918H8.394V2.886z M14.686,17.114H5.314V4.841h9.372V17.114z M12.525,7.306v7.344c0,0.252-0.207,0.459-0.46,0.459s-0.458-0.207-0.458-0.459V7.306c0-0.254,0.205-0.459,0.458-0.459S12.525,7.051,12.525,7.306M8.394,7.306v7.344c0,0.252-0.207,0.459-0.459,0.459s-0.459-0.207-0.459-0.459V7.306c0-0.254,0.207-0.459,0.459-0.459S8.394,7.051,8.394,7.306"></path>
                                            </svg>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif

@endsection
